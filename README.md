![Text Editor](https://user-images.githubusercontent.com/1669261/39924715-218a6b24-5553-11e8-8d04-69c4031ce777.png)

Text Editor
===========

> A text selection range API written in pure JavaScript, for modern browsers.

Demo
----

[Demo and Documentation](http://tovic.github.io/text-editor "View Demo")

Credits
-------

 - Logo by [@mirzazulfan](https://github.com/mirzazulfan)